#[path ="novela.user_management.rs"]
pub mod user_management;
pub use user_management::*;
use nrn_generator::PermissionAction as NRNPermissionAction;

//GrpcPermissionAction::from_i32(action)

impl Into<NRNPermissionAction> for PermissionAction {
    fn into(self) -> NRNPermissionAction {
        match self {
            PermissionAction::None => NRNPermissionAction::None ,
            PermissionAction::Read => NRNPermissionAction::Read ,
            PermissionAction::Write => NRNPermissionAction::Write ,
            PermissionAction::Create => NRNPermissionAction::Create ,
            PermissionAction::Delete => NRNPermissionAction::Delete ,
        }
    }
}

pub fn nrn_permission_action(code: i32) -> NRNPermissionAction {
    match PermissionAction::from_i32(code) {
        Some(action) => action.into(),
        None => NRNPermissionAction::None,
    }
}

//use tonic;
//tonic::include_proto!("novela.user_management");