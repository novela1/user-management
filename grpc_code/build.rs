fn main () -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::configure()
        .out_dir("./src")
        .emit_rerun_if_changed(false)
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(&["auth.proto"], &["./../protos"])?;
    
//    tonic_build::compile_protos("./../protos/auth.proto")?;
    Ok(())
}