# Load .env
ifneq ("$(wildcard .env)","")
    include .env
    export $(shell sed 's/=.*//' .env)
else
endif
.ONESHELL:
.EXPORT_ALL_VARIABLES:

WORKDIR := $(shell pwd)


help: ## Display help message
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'


init_service: ## Perform init operations (i.e. init DB)
	cargo run  -p nl-user-management --bin initer

check_app: ## Run check for the app
	cargo check -p nl-user-management

build_app: ## Run check for the app
	cargo build  -p nl-user-management
	
build_grpc_api: ## Run check for the app
	cargo build  -p nl_um_grpc

run_app: ## Run application (web server)
	cargo run -p nl-user-management --bin app 


run_with_watch: ## Run server and reload for each file changes
	cargo  watch -w app -c -x 'check -p nl-user-management --bins' -x 'build -p nl-user-management --bins ' -s 'touch .trigger' &
	cargo watch  --no-vcs-ignores -w .trigger -x 'run -p nl-user-management --bin app '