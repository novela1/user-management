use firestore::errors::FirestoreError as FError;
use firestore::*;
use regex::Regex;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fmt::Debug};
pub use nrn_generator::PermissionAction;

use super::{db, Model};


#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Permission {
    action: PermissionAction,
    pattern: String,

    #[serde(default = "Option::default")]
    comment: Option<String>,
}

impl Permission {
    pub fn new<S: Into<String>>(action: PermissionAction, pattern: S) -> Permission {
        Permission {
            action,
            pattern: pattern.into(),
            ..Default::default()
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct Role {
    pub name: String,

    pub readable_name: Option<String>,

    #[serde(default = "Option::default")]
    pub description: Option<String>,

    #[serde(default = "Vec::default")]
    pub assigned_to: Vec<String>,

    #[serde(default = "Vec::default")]
    pub permissions: Vec<Permission>,
}

impl Role {
    pub fn create<S: Into<String>>(name: S, readable_name: S) -> Role {
        Role {
            name: name.into(),
            readable_name: Some(readable_name.into()),
            ..Default::default()
        }
    }

    pub fn add_permission(&mut self, permission: Permission) -> &mut Self {
        self.permissions.push(permission);
        self
    }

    pub fn add_permission_set(&mut self, pattern: String, permissions: Vec<PermissionAction>) -> &mut Self {
        permissions.into_iter().for_each(|p| {
            self.add_permission(Permission::new(p, pattern.to_string()));
        });
        self
    }

    pub fn set_description<S: Into<String>>(&mut self, description: S) -> &mut Self {
        self.description = Some(description.into());
        self
    }

    pub fn add_assigne(&mut self, assigne: String) -> &mut Self {
        self.assigned_to.push(assigne);
        self
    }

    pub async fn get_roles_by_nrn(nrn_name: String) -> Result<Vec<Self>, FError> {
        let result: Vec<Self> = db()
            .fluent()
            .select()
            .from(Self::COLLECTION_NAME)
            .filter(move |q| q.for_all([q.field(path!(Role::assigned_to)).array_contains(nrn_name.to_string())]))
            .obj()
            .query()
            .await?;
        Ok(result)
    }

    pub async fn get_by_id(role_id: String) -> Result<Option<Self>, FError> {
        Ok(Self::get_one(HashMap::from([(path!(Role::name), role_id)])).await?)
    }

    pub async fn delete_by_id(role_name: String) -> Result<(), FError> {
        db().fluent().delete().from(Self::COLLECTION_NAME).document_id(role_name).execute().await?;
        Ok(())
    }

    pub async fn delete(&self) -> Result<(), FError> {
        Self::delete_by_id(self.name.to_string()).await
    }
}

pub trait RoleCheck {
    fn is_it_has_access(&self, target_resource_rnr: String, action: PermissionAction) -> bool;
}

impl RoleCheck for Role {
    fn is_it_has_access(&self, target_resource_rnr: String, action: PermissionAction) -> bool {
        (&self.permissions).iter().any(|p: &Permission| {
            p.action == action && Regex::new(p.pattern.as_str()).unwrap().is_match(target_resource_rnr.as_str())
        })
    }
}

impl RoleCheck for Vec<Role> {
    fn is_it_has_access(&self, target_resource_rnr: String, action: PermissionAction) -> bool {
        self.iter().any(|r| r.is_it_has_access(target_resource_rnr.to_string(), action))
    }
}

impl Model for Role {
    const COLLECTION_NAME: &'static str = "roles";

    fn id(&self) -> String {
        self.name.to_string()
    }
}
