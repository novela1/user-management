use crate::app_config;
use chrono::{Duration, Utc};
use firestore::errors::FirestoreError as FError;
use firestore::*;
use hex;
use jsonwebtoken::{
    decode as jwt_decode, encode as jwt_encode, Algorithm, DecodingKey, EncodingKey, Header, Validation,
};
use nrn_generator::nrn;
use openssl::sha::sha512;
use rand::distributions::{Alphanumeric, DistString};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

use crate::models::Model;

use super::{PermissionAction, Role, RoleCheck};

fn time_shift(delta: Duration) -> usize {
    Utc::now().checked_add_signed(delta).expect("valid timestamp").timestamp() as usize
}

#[derive(Serialize, Deserialize, Debug, Default)]
struct UserInvites {
    pub code: String,
    pub exp: usize,

    #[serde(default = "bool::default")]
    pub used: bool,
}

#[derive(Serialize, Deserialize, Debug, Default)]
pub struct User {
    user_id: String,

    #[serde(default = "Option::default")]
    secret: Option<String>,

    #[serde(default = "bool::default")]
    activated: bool,

    #[serde(default = "Vec::default")]
    invites: Vec<UserInvites>,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub enum AuthClaimTypes {
    #[default]
    Ghost,
    User,
}

#[derive(Debug, Serialize, Deserialize, Default)]
pub struct AuthClaim {
    pub sub: String,
    pub exp: usize,
    pub entity_type: AuthClaimTypes,
    pub entity_id: Option<String>,
}

impl Model for User {
    const COLLECTION_NAME: &'static str = "users";

    fn id(&self) -> String {
        self.user_id.to_string()
    }
}

impl User {
    pub fn rnr(&self) -> String {
        nrn().named_user(self.user_id.to_string()).get()
    }

    pub fn create(user_id: String) -> User {
        User {
            user_id: user_id.to_owned(),
            ..Default::default()
        }
    }

    pub fn activate(&mut self) -> &mut Self {
        self.activated = true;
        self
    }

    pub fn set_password(&mut self, password: String) -> &mut Self {
        self.secret = Some(hex::encode(sha512(password.as_bytes())));
        self
    }

    pub fn add_invite(&mut self) -> String {
        self.invites.push(UserInvites::new());
        self.invites.get(self.invites.len() - 1).unwrap().code.to_string()
    }

    pub async fn get_by_id(user_id: String) -> Result<Option<Self>, FError> {
        Ok(Self::get_one(HashMap::from([(path!(User::user_id), user_id)])).await?)
    }

    pub fn use_invitation(&mut self, token: String) -> bool {
        match self.invites.iter_mut().find(|x| x.code == token) {
            Some(invite) if !invite.used => {
                invite.used = true;
                true
            }
            _ => false,
        }
    }
}

impl From<User> for AuthClaim {
    fn from(value: User) -> Self {
        AuthClaim {
            sub: value.rnr(),
            exp: AuthClaim::d_exp(),
            entity_type: AuthClaimTypes::User,
            entity_id: Some(value.user_id),
        }
    }
}

impl AuthClaim {
    fn d_exp() -> usize {
        time_shift(Duration::hours(1))
    }

    pub fn from_jwt(token: String) -> Option<Self> {
        let conf = app_config::conf();
        let validation = Validation::new(Algorithm::HS512);
        let token_validation = jwt_decode::<Self>(
            &token,
            &DecodingKey::from_secret(conf.jwt_secret.as_bytes()),
            &validation,
        );
        match token_validation {
            Ok(t) => Some(t.claims),
            Err(_) => None,
        }
    }

    pub fn generate_jwt(&self) -> String {
        let conf = app_config::conf();
        let header = Header::new(Algorithm::HS512);
        match jwt_encode(&header, &self, &EncodingKey::from_secret(conf.jwt_secret.as_bytes())) {
            Ok(token) => token,
            Err(_) => panic!(), //ToDo in practice you would return the error
        }
    }

    pub async fn check_access(&self, target_resource_rnr: String, action: PermissionAction) -> Result<bool, FError> {
        let roles = Role::get_roles_by_nrn(self.sub.to_string()).await?;
        return Ok(roles.is_it_has_access(target_resource_rnr, action));
    }
}

impl UserInvites {
    pub fn new() -> UserInvites {
        UserInvites {
            exp: time_shift(Duration::days(1)),
            code: Alphanumeric.sample_string(&mut rand::thread_rng(), 32),
            used: false,
        }
    }
}

