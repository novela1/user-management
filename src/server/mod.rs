use tonic::{transport::Server, Request, Response, Status};


use novela::user_management::{
    JwtResponse, UserLoginForm,
    user_management_server::{ UserManagementServer, UserManagement},
};

use crate::models::{User, AuthClaim};

pub mod novela {
    pub mod user_management {
        tonic::include_proto!("novela.user_management");
    }
}

#[derive(Debug, Default)]
pub struct UMService {}


#[tonic::async_trait]
impl UserManagement for UMService {
    async fn login(&self,request: Request<UserLoginForm> ) ->  Result<Response<JwtResponse>, Status> {
        match User::get_by_id(request.into_inner().user_id).await {
            Ok(Some(user)) => Ok(Response::new(JwtResponse{jwt: AuthClaim::from(user).generate_jwt()})),
            Ok(None) => Err(Status::not_found("user not found")),
            _ => Err(Status::unknown("can not login with this user"))
        }
    }
}

pub fn um_server() -> UserManagementServer<UMService> {
    UserManagementServer::new(UMService::default())
}