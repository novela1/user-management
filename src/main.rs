mod app_config;
mod errors;
mod initer;
mod models;
mod server;
mod webapp;
use tonic::transport::Server;
#[macro_use]
extern crate rocket;
use tonic;

use app_config::conf;

use crate::errors::UMErrors;

pub mod novela {
    pub mod user_management {
        tonic::include_proto!("novela.user_management");
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    app_config::init();
    models::init_db().await?;

    if conf().init_um {
        initer::init().await?;
    } else {
        //        webapp::main().await?;
        Server::builder()
        .add_service(server::um_server())
        .serve(conf().get_adress())
        .await?;
    }

    Ok(())
}
