use crate::models::{AuthClaim, Model, PermissionAction, Role, User};
use nrn_generator::nrn;
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome};
use rocket::serde::{json::Json, Deserialize, Serialize};
use rocket::Request;
use std::fmt::Debug;

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct LoginRequest<'r> {
    user_id: &'r str,
    user_secret: &'r str,
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct JWTResponse {
    jwt: String,
}

#[derive(Debug)]
pub enum AuthErrors {
    Missing,
    Invalid,
    NotAuthorized,
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for AuthClaim {
    type Error = AuthErrors;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        match req.headers().get_one("Authorization") {
            None => Outcome::Failure((Status::BadRequest, AuthErrors::Missing)),
            Some(key) => {
                println!("{:?}", key);
                let prefix = "bearer ";
                if key[..(prefix.len())].to_lowercase() == prefix {
                    let claim = AuthClaim::from_jwt((&key[prefix.len()..]).to_string());
                    match claim {
                        Some(c) => Outcome::Success(c),
                        None => Outcome::Failure((Status::Forbidden, AuthErrors::NotAuthorized)),
                    }
                } else {
                    Outcome::Failure((Status::BadRequest, AuthErrors::Invalid))
                }
            }
        }
    }
}

#[post("/login", format = "json", data = "<body>")]
pub async fn login(body: Json<LoginRequest<'_>>) -> Result<Json<JWTResponse>, Status> {
    let user_query = User::get_by_id(body.user_id.to_string()).await;
    return match user_query {
        Ok(maybe_user) => match maybe_user {
            Some(user) => {
                let jwt_token = JWTResponse {
                    jwt: AuthClaim::from(user).generate_jwt(),
                };
                Ok(Json(jwt_token))
            }
            None => {
                println!("Not found...");
                Err(Status::NotFound)
            }
        },
        Err(_e) => Err(Status::InternalServerError),
    };
}

#[get("/whoami")]
pub async fn whoami(auth_claim: AuthClaim) -> (Status, Option<Json<AuthClaim>>) {
    (Status::Ok, Some(Json(auth_claim)))
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct CreateInviteRequest<'r> {
    user_id: &'r str,
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct CreateInviteResponse {
    user_id: String,
    token: String,
}

#[post("/invite/create", format = "json", data = "<body>")]
pub async fn create_invite(
    body: Json<CreateInviteRequest<'_>>,
    auth_claim: AuthClaim,
) -> Result<Json<CreateInviteResponse>, Status> {
    let body = body.into_inner();
    let mut user = match User::get_by_id(body.user_id.to_string()).await.expect("User not load") {
        None => {
            let user_nrn = nrn().named_user(body.user_id.to_string()).get();
            match auth_claim.check_access(user_nrn, PermissionAction::Create).await {
                Ok(true) => User::create(body.user_id.to_string()),
                Ok(false) => return Err(Status::Forbidden),
                Err(_) => return Err(Status::InternalServerError),
            }
        }
        Some(u) => u,
    };
    let invite = user.add_invite();
    user.save().await.unwrap();
    Ok(Json(CreateInviteResponse {
        user_id: body.user_id.to_string(),
        token: invite.to_string(),
    }))
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct UseInviteResponse {
    user_id: String,
    token: String,
    password: String,
}

#[post("/invite/use", format = "json", data = "<body>")]
pub async fn use_invite(body: Json<UseInviteResponse>) -> Result<Json<JWTResponse>, Status> {
    let body = body.into_inner();
    let mut new_user = match User::get_by_id(body.user_id.to_string()).await {
        Ok(Some(user)) => user,
        _ => return Err(Status::NotFound),
    };
    if !new_user.use_invitation(body.token) {
        return Err(Status::NotFound);
    };
    new_user.activate().set_password(body.password);
    new_user.save().await.unwrap();
    let jwt = AuthClaim::from(new_user).generate_jwt();
    Ok(Json(JWTResponse { jwt }))
}

#[derive(Serialize, Deserialize)]
#[serde(crate = "rocket::serde")]
pub struct CheckPermissionRequest {
    target_nrn: String,
    action: PermissionAction,
}

#[post("/check_permission", format = "json", data = "<body>")]
pub async fn check_permission(body: Json<CheckPermissionRequest>, auth_claim: AuthClaim) -> Status {
    match auth_claim.check_access(body.target_nrn.to_string(), body.action).await {
        Ok(true) => Status::Ok,
        _ => Status::Unauthorized,
    }
}

#[post("/roles", format = "json", data = "<body>")]
pub async fn create_mew_role(body: Json<Role>, auth_claim: AuthClaim) -> Status {
    match auth_claim.check_access(nrn().named_role(body.name.to_string()).get(), PermissionAction::Create).await {
        Ok(true) => match body.save().await {
            Ok(()) => Status::Ok,
            Err(_) => Status::InternalServerError,
        },
        _ => Status::Unauthorized,
    }
}

#[delete("/roles/<role_name>")]
pub async fn delete_role(role_name: &str, auth_claim: AuthClaim) -> Status {
    match auth_claim.check_access(nrn().named_role(role_name).get(), PermissionAction::Delete).await {
        Ok(true) => {}
        _ => return Status::Unauthorized,
    };
    match Role::get_by_id(role_name.to_string()).await {
        Ok(Some(role)) => match role.delete().await {
            Ok(_) => Status::Ok,
            _ => Status::InternalServerError,
        },
        Ok(None) => Status::NotFound,
        _ => Status::InternalServerError,
    }
}
