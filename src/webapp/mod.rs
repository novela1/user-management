use crate::errors::UMErrors;

pub mod controllers;


#[get("/")]
async fn index() -> &'static str {
    "ok"
}

pub async fn main() -> Result<(), UMErrors>  {
    let _rocket = rocket::build()
        .mount("/", routes![index])
        .mount("/api/v1", routes![
                    controllers::login,
                    controllers::whoami,
                    controllers::create_invite,
                    controllers::use_invite,
                    controllers::check_permission,
                    controllers::create_mew_role,
                    controllers::delete_role,
                ])
        .launch()
        .await?;

    Ok(())
}
