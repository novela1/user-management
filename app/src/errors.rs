use std::fmt;

use firestore::errors::FirestoreError;

#[derive(Debug)]
pub enum UMErrors {
    FirestoreError(FirestoreError),
}


impl From<FirestoreError> for UMErrors{
    fn from(value: FirestoreError) -> Self {
        Self::FirestoreError(value)
    }
}

impl fmt::Display for UMErrors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,"{}",format!("{:?}",self))
    }
}

impl std::error::Error for UMErrors {
}