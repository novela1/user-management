use std::net::SocketAddr;

use serde::{Deserialize};
use figment::{Figment, providers::Env};
use once_cell::sync::OnceCell;


static CONF_INSTANCE: OnceCell<AppConfig> = OnceCell::new();


#[derive(Deserialize, Debug)]
pub struct AppConfig {
    pub jwt_secret: String,

    #[serde(default = "default_address")]
    pub host: String,

    #[serde(default = "default_port")]
    pub port: usize,

    #[serde(default)]
    pub admin_username: String,

    #[serde(default)]
    pub admin_password: String,

    #[serde(default = "bool::default")]
    pub is_create_admin: bool,
}

fn default_address()->String {"[::1]".to_string()}

fn default_port()->usize {8730}

pub fn init() {
    CONF_INSTANCE.set(Figment::from(Env::prefixed("NL_UM_")).extract().unwrap()).unwrap();
}

pub fn conf() -> &'static AppConfig {
    CONF_INSTANCE.get().expect("Config not inited")
}

impl AppConfig {
   pub fn get_adress(&self) -> SocketAddr{
        format!("{}:{}", self.host, self.port).parse().unwrap()
    }
}
