#![allow(dead_code)]

mod app_config;
mod errors;
mod models;
mod server;
use tonic;
use tonic::transport::Server;

use app_config::conf;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    app_config::init();
    models::init_db().await?;
    Server::builder().add_service(server::um_server()).serve(conf().get_adress()).await?;
    Ok(())
}
