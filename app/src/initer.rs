#![allow(dead_code)]

mod app_config;
mod errors;
mod models;
mod server;


use errors::UMErrors;
use models::{Model, PermissionAction, Role, User};
use nrn_generator;
use nrn_generator::{nrn, NRNChunk};
use std::process::Command;

pub async fn init() -> Result<(), UMErrors> {
    let conf = app_config::conf();
    nrn().any_user();
    nrn().named_user("sd").invite().get();

    if conf.is_create_admin && User::get_by_id(conf.admin_username.to_string()).await?.is_none() {
        create_admin_user().await?;
    };

    create_index_for_db();
    Ok(())
}

fn create_index_for_db() {
    let run = |cmd: &str| Command::new("sh").arg("-c").arg(cmd).output().expect("failed to execute process");
    run("gcloud firestore indexes fields update user_id --collection-group=users --index=order=ascending");
    run("gcloud firestore indexes fields update assigned_to --collection-group=roles --index=array-config=contains");
}

async fn create_admin_user() -> Result<(), UMErrors> {
    let conf = app_config::conf();
    let mut admin_user = User::create(conf.admin_username.to_string());
    admin_user.set_password(conf.admin_password.to_string()).activate();
    admin_user.save().await?;

    let mut role = Role::create("global-admin");
    role.set_readable_name("Global admin");
    role.add_permission_set(
        nrn().any(),
        vec![
            PermissionAction::Read,
            PermissionAction::Write,
            PermissionAction::Create,
            PermissionAction::Delete,
        ],
    );
    role.set_description("This role is used for access to any resources in the system. Some sort of the global admin");
    role.add_assigne(admin_user.rnr());
    role.save().await?;
    Ok(())
}


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    app_config::init();
    models::init_db().await?;
    init().await?;

    Ok(())
}

