use firestore::errors::FirestoreError;
use tonic::{
    metadata::{Ascii, MetadataMap, MetadataValue},
    Request, Response, Status,
};

use nl_um_grpc::{
    nrn_permission_action,
    user_management_server::{UserManagement, UserManagementServer},
    CheckPermissionRequest, CreateInviteRequest, CreateInviteResponse, DeleteRoleRequest, JwtResponse, Null,
    RoleUpsertRequest, UseInviteRequest, UserLoginForm,
};

use nrn_generator::{nrn, NRNChunk, PermissionAction};

use crate::models::{AuthClaim, Model, Permission, Role, User};

struct TokenCheck {
    auth_header: Option<MetadataValue<Ascii>>,
    target_role: Option<String>,
    action: Option<PermissionAction>,
}

impl TokenCheck {
    fn set_target_role(&mut self, role: String) -> &mut Self {
        self.target_role = Some(role);
        self
    }

    fn set_action(&mut self, action: PermissionAction) -> &mut Self {
        self.action = Some(action);
        self
    }

    async fn check(&self) -> Result<(), Status> {
        match self.auth_header.as_ref() {
            Some(raw_token) => match raw_token.to_str() {
                Ok(token) => match AuthClaim::from_jwt((token["bearer ".len()..]).to_string()) {
                    Some(claim) => match claim
                        .check_access(self.target_role.as_ref().unwrap().to_string(), self.action.unwrap())
                        .await
                    {
                        Ok(true) => Ok(()),
                        _ => Err(Status::permission_denied("")),
                    },
                    None => Err(Status::unauthenticated("")),
                },
                Err(_) => Err(Status::unauthenticated("")),
            },
            None => Err(Status::unauthenticated("")),
        }
    }
}

impl<'a, T> From<&'a Request<T>> for TokenCheck {
    fn from(req: &'a Request<T>) -> Self {
        TokenCheck::from(req.metadata())
    }
}

impl From<MetadataMap> for TokenCheck {
    fn from(meta: MetadataMap) -> Self {
        TokenCheck::from(&meta)
    }
}
impl From<&MetadataMap> for TokenCheck {
    fn from(meta: &MetadataMap) -> Self {
        TokenCheck { auth_header: meta.get("Authorization").cloned(), target_role: Option::None, action: Option::None }
    }
}

trait GeneralFirestoreErrorForTokio<T> {
    fn with_status(self, status: Status) -> Result<T, Status>;
}

impl<T> GeneralFirestoreErrorForTokio<T> for Result<T, FirestoreError> {
    fn with_status(self, status: Status) -> Result<T, Status> {
        match self {
            Ok(ok) => Ok(ok),
            Err(_) => Err(status),
        }
    }
}

#[derive(Debug, Default)]
pub struct UMService {}

#[tonic::async_trait]
impl UserManagement for UMService {
    async fn login(&self, request: Request<UserLoginForm>) -> Result<Response<JwtResponse>, Status> {
        match User::get_by_id(request.into_inner().user_id).await {
            Ok(Some(user)) => Ok(Response::new(JwtResponse { jwt: AuthClaim::from(user).generate_jwt() })),
            Ok(None) => Err(Status::not_found("user not found")),
            _ => Err(Status::unknown("can not login with this user")),
        }
    }

    async fn create_invite(
        &self,
        request: Request<CreateInviteRequest>,
    ) -> Result<Response<CreateInviteResponse>, Status> {
        let (meta, _, CreateInviteRequest { user_id }) = request.into_parts();
        TokenCheck::from(&meta)
            .set_target_role(nrn().named_user(user_id.to_string()).invite().get())
            .set_action(PermissionAction::Create)
            .check()
            .await?;

        let mut user = match User::get_by_id(user_id.to_string()).await {
            Ok(None) => User::create(user_id.to_string()),
            Ok(Some(u)) => u,
            _ => return Err(Status::unknown("issue with fetch/create user")),
        };
        let invite = user.add_invite();
        user.save().await.with_status(Status::aborted("Invite not saved".to_string()))?;
        Ok(Response::new(CreateInviteResponse { user_id: user_id.to_string(), token: invite.to_string() }))
    }

    async fn use_invite(&self, request: Request<UseInviteRequest>) -> Result<Response<JwtResponse>, Status> {
        let UseInviteRequest { user_id, token, password } = request.into_inner();
        let mut new_user = match User::get_by_id(user_id.to_string()).await {
            Ok(Some(user)) => user,
            _ => return Err(Status::not_found(format!("user with id {user_id} not found"))),
        };
        if !new_user.use_invitation(token) {
            return Err(Status::not_found("invite not found"));
        };
        new_user.activate().set_password(password);
        new_user.save().await.with_status(Status::unknown("Can not update user in DB"))?;
        Ok(Response::new(JwtResponse { jwt: AuthClaim::from(new_user).generate_jwt() }))
    }

    async fn check_permission_action(
        &self,
        request: Request<CheckPermissionRequest>,
    ) -> Result<Response<Null>, Status> {
        let (meta, _, CheckPermissionRequest { target_nrn, action }) = request.into_parts();
        TokenCheck::from(&meta)
            .set_target_role(target_nrn.to_string())
            .set_action(nrn_permission_action(action))
            .check()
            .await?;
        Ok(Response::new(Null {}))
    }

    async fn upsert_role(&self, request: Request<RoleUpsertRequest>) -> Result<Response<Null>, Status> {
        let (meta, _, RoleUpsertRequest { name, readable_name, description, assigned_to, permissions }) =
            request.into_parts();
        let mut checker = TokenCheck::from(meta);
        checker.set_target_role(nrn().named_role(name.to_string()).get());
        let mut role = match Role::get_by_id(name.to_string()).await {
            Ok(Some(r)) if checker.set_action(PermissionAction::Write).check().await? == () => r,
            _ if checker.set_action(PermissionAction::Create).check().await? == () => Role::create(name),
            _ => return Err(Status::unknown("")),
        };

        if let Some(r_name) = readable_name {
            role.set_readable_name(r_name.to_string());
        }

        if let Some(d) = description {
            role.set_description(d);
        }

        assigned_to.iter().for_each(|a| {
            role.add_assigne(a.to_string());
        });
        permissions.iter().for_each(|p| {
            role.add_permission(Permission::new(nrn_permission_action(p.action), p.pattern.to_string()));
        });
        role.save().await.with_status(Status::unknown("Can not update role in DB"))?;
        Ok(Response::new(Null {}))
    }

    async fn delete_role(&self, request: Request<DeleteRoleRequest>) -> Result<Response<Null>, Status> {
        let (meta, _, DeleteRoleRequest { role_name }) = request.into_parts();
        TokenCheck::from(meta)
            .set_target_role(nrn().named_user(role_name.to_string()).get())
            .set_action(PermissionAction::Delete)
            .check().await?;
        match Role::get_by_id(role_name).await {
            Ok(Some(role)) => match role.delete().await {
                Ok(_) =>  Ok(Response::new(Null {})),
                _ => Err(Status::unknown("some error happens"))
            },
            _ => return Err(Status::not_found(""))
        }
    }
}

pub fn um_server() -> UserManagementServer<UMService> {
    UserManagementServer::new(UMService::default())
}
